# Lab work Themes

* L1 - Finding function solutions (f(x) = 0)
* L2 - Solving function system, optimization using gradient descend
* L3 - Interpolation, approximation
* L4 - Solving differential equations
