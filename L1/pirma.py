import numpy as np
import matplotlib.pyplot as plt

# f(x)
f = lambda x:\
    1.03 * x**5 - 2.91 * x**4 - 1.44 * x**3 + 5.56 * x**2 - 0.36 * x - 1.13

df = lambda x:\
    1.03 * 5 * x**4 - 2.91 * 4 * x**3 - 1.44 * 3 * x**2 + 5.56 * 2 * x - 0.36

# g(x)
g = lambda x: np.sin(x) * np.log(x) - x / 6

dg = lambda x: np.sin(x) / x * np.log(x) * np.cos(x) - 1 / 6


def scanning(f, xp, xg, h):
    '''
    f - function
    xp - scan region beginning
    xg - scan region end
    h - scan step
    '''
    intervals = []
    for x in np.arange(xp, xg, h):
        x1 = x + h
        if (np.sign(f(x)) != np.sign(f(x1))):
            intervals.append((x, x1))
    return intervals


def chords(f, x_min, x_max, epsilon):
    k = abs(f(x_min) / f(x_max))
    x_mid = (x_min + k * x_max) / (1 + k)

    i = 0
    while abs(f(x_mid)) > epsilon:
        if (f(x_min) * f(x_mid) > 0):
            x_min = x_mid
        else:
            x_max = x_mid

        k = abs(f(x_min) / f(x_max))
        x_mid = (x_min + k * x_max) / (1 + k)
        i += 1

    return x_mid, abs(f(x_mid)), i


def tangent(f, df, x_min, x_max, i_max, epsilon):
    x0 = x_min

    for i in range(i_max):
        y = f(x0)
        dy = df(x0)

        if abs(y) < epsilon:
            break

        x0 -= .2 * y / dy

        if x0 < x_min:
            x0 = x_min
        elif x0 > x_max:
            x0 = x_max

    return x0, abs(f(x0)), i


def decreasing_step(f, x_min, x_max, step, epsilon):
    x0 = x_min

    i = 0
    while abs(f(x0)) > epsilon:
        if f(x0) * f(x0 - step) <= 0:
            step /= -2
        x0 += step
        i += 1

    return x0, abs(f(x0)), i


intervals_f = scanning(f, -3.5, 4, .5)
intervals_g = scanning(g, 1, 20, .5)

xf = np.arange(-1.5, 2.75, 0.01)
xg = np.arange(1, 20, 0.01)

froots = []
groots = []

print(80*'-')

print("f(x) intervalai:")
for i in intervals_f:
    print('\t', i)

print("Stygų metodas:")
for i in intervals_f:
    x, y, c = chords(f, *i, 1e-9)
    froots.append(x)
    print(f"{i} \t {x:7.3f} \t {y:.3e} \t {c:>3} ")

print("Niutono metodas:")
for i in intervals_f:
    x, y, c = tangent(f, df, *i, 1000, 1e-9)
    print(f"{i} \t {x:7.3f} \t {y:.3e} \t {c:>3} ")

print("Skenavimo su mažėjančiu žingsniu metodas:")
for i in intervals_f:
    x, y, c = decreasing_step(f, *i, 0.1, 1e-9)
    print(f"{i} \t {x:7.3f} \t {y:.3e} \t {c:>3} ")

print(80*'-')

print("g(x) intervalai:")
for i in intervals_g:
    print('\t', i)

print("Stygų metodas:")
for i in intervals_g:
    x, y, c = chords(g, *i, 1e-9)
    groots.append(x)
    print(f"{i} \t {x:7.3f} \t {y:.3e} \t {c:>3} ")

print("Niutono metodas:")
for i in intervals_g:
    x, y, c = tangent(g, dg, *i, 1000, 1e-9)
    print(f"{i} \t {x:7.3f} \t {y:.3e} \t {c:>3} ")

print("Skenavimo su mažėjančiu žingsniu metodas:")
for i in intervals_g:
    x, y, c = decreasing_step(g, *i, 0.1, 1e-9)
    print(f"{i} \t {x:7.3f} \t {y:.3e} \t {c:>3} ")

def plot_begin():
    plt.grid()
    plt.axhline(c='k')

def plot_end(name):
    plt.tight_layout()
    plt.savefig(name, dpi=300)


plot_begin()
plt.plot(xf, f(xf))
plt.ylabel("$f(x)$", loc="top", rotation=0)
plt.xlabel("$x$")
plt.plot((-6.4, 6.4), np.zeros(2), lw=5, alpha=.6, c='crimson')
plt.plot((-3.32, 3.83), np.zeros(2), lw=7, alpha=.6, c='steelblue')
plt.ylim(-5.75, 4)
plot_end("froots.png")
plt.clf()

# f plot
plot_begin()
plt.plot(xf, f(xf))
plt.ylabel("$f(x)$", loc="top", rotation=0)
plt.xlabel("$x$")
for i in intervals_f:
    plt.plot(i, np.zeros(2), lw=5, alpha=.5)

plt.ylim(-5.75, 4)
plot_end("fscan.png")

plt.scatter(froots, np.zeros(len(froots)), c='firebrick', zorder=10)
plot_end("fscanroots.png")
plt.clf()

# g plot
plot_begin()
plt.plot(xg, g(xg), c='coral')
plt.ylabel("$g(x)$", loc="top", rotation=0)
plt.xlabel("$x$")
plt.xlim(1, 20)
for i in intervals_g:
    plt.plot(i, np.zeros(2), lw=5, alpha=.5)

plot_end("gscan.png")

plt.scatter(groots, np.zeros(len(groots)), c='firebrick', zorder=10)
plot_end("gscanroots.png")
plt.clf()
