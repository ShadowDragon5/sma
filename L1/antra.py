import numpy as np
import matplotlib.pyplot as plt


def chords(f, x_min, x_max, epsilon):
    k = abs(f(x_min) / f(x_max))
    x_mid = (x_min + k * x_max) / (1 + k)

    i = 0
    while abs(f(x_mid)) > epsilon:
        if (f(x_min) * f(x_mid) > 0):
            x_min = x_mid
        else:
            x_max = x_mid

        k = abs(f(x_min) / f(x_max))
        x_mid = (x_min + k * x_max) / (1 + k)
        i += 1

    return x_mid, abs(f(x_mid)), i

T0 = 400
TA = 320
t1 = 30
T1 = 344

f = lambda x: (T0 - TA) * np.exp(t1 * x) + TA - T1

fx = np.arange(-0.1, 0, .001)

x, y, c = chords(f, -0.1, 0, 1e-9)
print(f"(-0.1, 0) \t {x:.3f} \t {y:.3e} \t {c}")

plt.grid()
plt.axhline(c='k')
plt.plot(fx, f(fx))
plt.scatter(x, y, c='firebrick', zorder=10)
plt.xlabel("$x$")
plt.tight_layout()
plt.savefig("antra.png", dpi=300)
