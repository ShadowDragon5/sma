'''
Paprastųjų diferencialinių lygčių sprendimas. 5 Uždavinys
'''
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint

def stage(x, y, dx, y_n):
    return y_n + dx * diff_function(y, x)

def Eulers(x, y, dx):
    return stage(x, y, dx, y)

def IV_Runge_Kutta(x, y, dx):
    dx_h = dx / 2
    y1 = Eulers(x, y, dx_h)
    y2 = stage(x + dx_h, y1, dx_h, y)
    y3 = stage(x + dx_h, y2, dx, y)
    y4 = y + dx/6 * (diff_function(y, x) + 2 * diff_function(y1, x + dx/2) +
        2 * diff_function(y2, x + dx/2) + diff_function(y3, x + dx))

    return y4

def T_A(t):
    '''
    Aplinkos temperatūros kitimo funkcija
    t - laiko momentas
    '''
    # aplinkos temperatūros
    T_A1 = 320
    T_A2 = 460

    T_h = 20        # pusė periodo
    t_s = 30
    if t < t_s:
        return T_A1
    if t < t_s + T_h:
        return T_A1 + (T_A2 - T_A1) / 2 * (1 - np.cos(np.pi / T_h * (t - t_s)))
    return T_A2

def k(T):
    '''
    Proporcingumo koeficiento funkcija
    '''
    return -0.01 - 0.16 * (T - 273) / 100 - 0.04 * ((T - 273) / 100) ** 2

def diff_function(T, t):
    '''
    Diferencialinė lygtis
    '''
    return k(T) * (T - T_A(t))

def plot_end(name):
    plt.grid()
    plt.legend()
    plt.xlabel("$t$, s")
    plt.ylabel("$T$, K")
    plt.tight_layout()
    plt.savefig(f"img/{name}.png", dpi=300)
    plt.clf()


t_max = 80

# pradinė kūno temperatūra
T_1 = 400
#T_1 = 270

for dx in [4, 2, 1, 0.5]:
    eul = [T_1]
    for t in np.arange(0, t_max, dx):
        eul.append(Eulers(t, eul[-1], dx))
    t = np.arange(0, t_max + dx, dx)
    plt.plot(t, eul, label=f"$\Delta x$={dx}")

plot_end(f"eulers{T_1}")

for dx in [8, 4, 2, 1, 0.5]:
    RK = [T_1]
    for t in np.arange(0, t_max, dx):
        T = IV_Runge_Kutta(t, RK[-1], dx)
        RK.append(T)
    t = np.arange(0, t_max + dx, dx)
    plt.plot(t, RK, label=f"$\Delta x$={dx}")

plot_end(f"runge_kutta{T_1}")

# Uždavinio sprendimas
dx = 0.5
t = np.arange(0, t_max + dx, dx)

RK = [T_1]
once = True
for ti in np.arange(0, t_max, dx):
    T = IV_Runge_Kutta(ti, RK[-1], dx)
    RK.append(T)
    if (abs(T - T_A(ti)) < 10e-1 and once):
        print(f"Kūno temp. pasiekė aplinkos temp.: {ti}s")
        once = False

plt.plot(t, list(map(T_A, t)), label="$T_A(t)$")
plt.plot(t, RK, label="$T(t)$")
plot_end(f"solution{T_1}")

# sprendžiama su standartine funkcija
real_solution = odeint(diff_function, T_1, t)

plt.plot(t, eul, c='b', label="Eulerio")
plt.plot(t, RK, c='g', label="IV eilės Rungės ir Kutos")
plt.plot(t, real_solution, c='crimson', label="odeint")
plot_end(f"comparison{T_1}")
