'''
2. Netiesiniu lygčių sistemų sprendimas. Greičiausio nusileidimo metodas
'''

import numpy as np
import matplotlib.pyplot as plt
np.set_printoptions(precision=3, suppress=True)

def plot_begin():
    plt.grid()
    plt.xlabel("$x_1$")
    plt.ylabel("$x_2$", rotation=0)

def plot3d_begin():
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.set_xlabel("$x_1$")
    ax.set_ylabel("$x_2$")
    ax.set_zlabel("z")
    ax.view_init(15, 60)
    return ax

def plot_end(name):
    plt.tight_layout()
    plt.savefig("img/" + name, dpi=300)
    plt.clf()


def target(F, X):
    return 0.5 * sum([f(*X) ** 2 for f in F])

def gradient(X, F, J):
    fs = [f(*X) for f in F]
    return fs @ J(*X)

def gradient_descent(start, F, J, max_iter, delta_s, eps):
    '''
    start - tuple of starting point coordinates
    F - list of functions
    J - Jacobian matrix function
    max_iter - maximum number of iterations
    delta_s - step size
    eps - answer precision
    '''
    X = np.matrix(start).astype(np.float)
    last = target(F, X.T)
    grad = gradient(start, F, J)
    g = grad / np.linalg.norm(grad) * delta_s

    for i in range(1, max_iter+1):
        X -= g

        curr = target(F, X.T)
        if (last < curr):
            X += g
            delta_s /= 10.0
            grad = gradient(start, F, J)
            g = grad / np.linalg.norm(grad) * delta_s
        else:
            last = curr

        if (last < eps):
            break

    print(f"{start}  \t{i:>4} \t{X} \t{last.item():.3}")


def jacobian1(x1, x2):
    return np.matrix([
        [0.3 * x1 ** 2 - 0.3 * x2 ** 2, -0.6 * x1 * x2],
        [   2 * x1 - 5 * np.sin(x1),        2 * x2    ]])

# 1.
Z1 = lambda x1, x2: 0.1 * x1 ** 3 - 0.3 * x1 * x2 ** 2
Z2 = lambda x1, x2: x1 ** 2 + x2 ** 2 + 5 * np.cos(x1) - 16
F1 = [Z1, Z2]

x1 = x2 = np.arange(-5, 5, .5)
X1, X2 = np.meshgrid(x1, x2)

ax = plot3d_begin()
ax.plot_surface(X1, X2, Z1(X1, X2))
plot_end("Z1.png")

ax = plot3d_begin()
ax.plot_surface(X1, X2, Z2(X1, X2), color='orange')
plot_end("Z2.png")

plot_begin()
plt.contour(X1, X2, Z1(X1, X2), [0], colors='blue')
plt.contour(X1, X2, Z2(X1, X2), [0], colors='orange')
plot_end("zero.png")

print("Pirmoji lygčių sistema:")
for p in [(3.5, 2), (-3.4, 2.01), (-4, -2), (4, -2), (0, 3), (0, -3)]:
    gradient_descent(p, F1, jacobian1, 1000, 0.05, 1e-9)

# 2.
def jacobian2(x1, x2, x3, x4):
    return np.matrix([
        [ 2,        2,         -3,           0],
        [x2,        x1,         0,          -2],
        [ 0, -8 * x2 + x3, x2 + 9 * x3 ** 2, 0],
        [ 5,       -6,          1,           3]])

F2 = [
    lambda x1, x2, x3, x4: 2 * x1 + 2 * x2 - 3 * x3 - 32,
    lambda x1, x2, x3, x4: x1 * x2 - 2 * x4 - 12,
    lambda x1, x2, x3, x4: -4 * x2 ** 2 + x2 * x3 + 3 * x3 ** 3 + 676,
    lambda x1, x2, x3, x4: 5 * x1 - 6 * x2 + x3 + 3 * x4 - 4
]

print("Antroji lygčių sistema:")
gradient_descent((4.8, 2, -6, -1), F2, jacobian2, 1000, 0.05, 1e-9)
