'''
1. Tiesiniu lygčių sistemos sprendimas. QR skaidos metodas
'''

import numpy as np
np.set_printoptions(precision=3, suppress=True)

A = np.matrix([[1, 2, 1, 0],
                [2, 5, 0, 4],
                [14, -8, 4, 1],
                [4, 10, 0, 8]]).astype(np.float)

b = np.matrix([-4, 3, 7, 2]).T.astype(np.float)

n = len(A)
Q = np.eye(n)

for i in range(n-1):
    print(i+1, "iteracija", '-'*50)
    z = A[i:, i]
    zp = np.zeros_like(z)
    zp[0] = np.linalg.norm(z)

    omega = z - zp
    omega = omega / np.linalg.norm(omega)

    Qi = np.eye(n-i) - 2 * omega * omega.T
    A[i:,] = Qi @ A[i:,]

    Q = Q @ np.vstack(( np.hstack(( np.eye(i), np.zeros((i, n-i)) )),
                    np.hstack(( np.zeros((n-i, i)), Qi )) ))
    print(f"A:\n{A}")
    print(f"Q:\n{Q}")

b1 = Q.T @ b
x = np.zeros((n, 1))
print('-'*62)
for i in range(n-1, -1, -1):
    x[i,] = (b1[i,] - A[i, i+1:] * x[i+1:,]) / A[i,i]

print(f"x:\n{x}")
