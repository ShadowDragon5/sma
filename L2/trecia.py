'''
3. Optimizavimas
'''
import numpy as np
import matplotlib.pyplot as plt
import random as rng


def plot_begin():
    plt.grid()
    plt.axhline(c='k')
    plt.axvline(c='k')
    plt.xlabel("$x$")
    plt.ylabel("$y$", rotation=0)

    # circle
    theta = np.linspace(0, 2 * np.pi, 100)
    plt.plot(S * np.cos(theta), S * np.sin(theta), c='crimson', alpha=.5)


def plot_end(name):
    plt.tight_layout()
    plt.savefig("img/" + name, dpi=300)
    plt.clf()


def average_distance(points):
    distance = 0
    for i, p1 in enumerate(points[:-1]):
        for p2 in points[i+1:]:
            distance += np.linalg.norm(p1 - p2)

    n = len(points)
    return distance / (n * (n-1))

def PSI(N, M, S, d_vid):
    loss = 0

    for i, m in enumerate(M.T):
        for mn in np.concatenate((M.T, N.T))[i+1:]:
            loss += (np.linalg.norm(m - mn) - d_vid) ** 2
        loss += (np.linalg.norm(m) - S) ** 2

    return loss

def gradient(N, M, S, d_vid):
    h = 0.001

    n = M.shape[1]
    grad = np.zeros_like(M)
    initial_loss = PSI(N, M, S, d_vid)

    for i in range(n):
        hM = M.copy()
        hM[0][i] += h
        loss = PSI(N, hM, S, d_vid)
        grad[0][i] = (loss - initial_loss) / h

    for i in range(n):
        hM = M.copy()
        hM[1][i] += h
        loss = PSI(N, hM, S, d_vid)
        grad[1][i] = (loss - initial_loss) / h

    return grad

def gradient_descent(N, M, max_iter, delta_s, eps):
    d_vid = average_distance(np.concatenate((N.T, M.T)))
    last = PSI(N, M, S, d_vid)
    grad = gradient(N, M, S, d_vid)
    g = grad / np.linalg.norm(grad) * delta_s
    losses = [last]

    for i in range(1, max_iter+1):
        M -= g
        grad = gradient(N, M, S, d_vid)
        g = grad / np.linalg.norm(grad) * delta_s

        curr = PSI(N, M, S, d_vid)
        losses.append(curr)
        '''
        if (last < curr):
            M += g
            delta_s /= 2
            grad = gradient(N, M, S, d_vid)
            g = grad / np.linalg.norm(grad) * delta_s
        else:
            last = curr
        '''

        if (last < eps):
            break

    return M, losses


rng.seed(10)

n = 10
m = 10
S = 3

X = [rng.uniform(-10, 10) for _ in range(n)]
Y = [rng.uniform(-10, 10) for _ in range(n)]
N = np.array([X, Y])

Xadded = [rng.uniform(-10, 10) for _ in range(m)]
Yadded = [rng.uniform(-10, 10) for _ in range(m)]
M = np.array([Xadded, Yadded])

Mnew, losses = gradient_descent(N.copy(), M.copy(), 1000, 0.1, 1e-6)

plot_begin()
plt.scatter(N[0], N[1])
plt.scatter(M[0], M[1], c='r', marker='*')
plt.scatter(Mnew[0], Mnew[1], c='g', marker='*')
plot_end("optimization_after.png")

plt.grid()
plt.xlabel("Iteracijos")
plt.ylabel("Tikslo funkcijos reikšmė")

plt.plot(losses)

plot_end("losses.png")
