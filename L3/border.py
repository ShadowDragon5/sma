'''
III užduotis. Parametrinis interpoliavimas.
'''
import numpy as np
import shapefile as shp
import matplotlib.pyplot as plt
from global_spline import GlobalSpline


sf = shp.Reader("countries/ne_10m_admin_0_countries")

country_id = [i for i, x in enumerate(sf.records()) if x.NAME_EN == 'Austria'][0]
X, Y = zip(*sf.shape(country_id).points)
X, Y = np.array(X), np.array(Y)

for n in [10, 20, 50, 100]:
    idx = np.linspace(0, len(X)-1, n, dtype=int)
    t = np.arange(n)

    gsx = GlobalSpline(t, X[idx], True)
    gsy = GlobalSpline(t, Y[idx], True)

    _, Xval = gsx.eval()
    _, Yval = gsy.eval()

    plt.clf()
    plt.axis('off')
    plt.plot(Xval, Yval)
    plt.plot(X, Y, 'k.', alpha=.1)
    plt.scatter(X[idx], Y[idx])
    plt.title(f"{n} taškų")
    plt.tight_layout()
    plt.savefig(f"img/border{n}.png", dpi=300)
