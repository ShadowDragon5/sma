'''
II užduotis. Interpoliavimas daugianariu ir splainu per duotus taškus
'''
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from global_spline import GlobalSpline
from chebyshev import Chebyshev


def plot_begin():
    plt.grid()
    plt.xlabel("Mėnuo")
    plt.ylabel("Temperatūra, $\degree$C")

def plot_end(name):
    plt.tight_layout()
    plt.savefig(f"img/{name}.png", dpi=300)
    plt.clf()


df = pd.read_csv("data/tas_1991_2016_AUT.csv", sep=', ', engine='python')

Y = df[df.Year == 2014].Temperature.to_numpy()
X = np.arange(1, 13)

ch = Chebyshev(X, Y)
gs = GlobalSpline(X, Y)

smooth = np.linspace(1, 12, 1000)
Ych = ch.eval(smooth)
Xgs, Ygs = gs.eval(100)

plot_begin()
plt.scatter(X, Y)
plt.plot(Xgs, Ygs)
plot_end("temps_spline")

plot_begin()
plt.scatter(X, Y)
plt.plot(smooth, Ych)
plot_end("temps_chebyshev")
