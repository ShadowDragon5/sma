'''
I užduotis. Interpoliavimas daugianariu.
'''
import numpy as np
from matplotlib import pyplot as plt
from chebyshev import Chebyshev


def calc_and_plot_graph(X, interval, name):
    smooth = np.linspace(*interval, 200)
    yreal = [f(x) for x in smooth]

    Y = f(X)
    ch = Chebyshev(X, Y)
    yval = ch.eval(smooth)

    plt.clf()
    plt.grid()
    plt.plot(smooth, yreal, label="duota f-ja")
    plt.plot(smooth, yval, c='g', label="interpoliacinė f-ja")
    plt.plot(smooth, yreal - yval, c='crimson', label="netiktis")
    plt.scatter(X, Y, c='firebrick')

    plt.xlabel("$x$")
    plt.ylabel("$y$", rotation=0)
    plt.legend()
    plt.tight_layout()
    plt.savefig(f"img/{name}.png", dpi=300)


f = lambda x: np.cos(2 * x) / (np.sin(2 * x) + 1.5) - x / 5

interval = (-2, 3)
n = 15              # taškų skaičius

X = np.linspace(*interval, n)

calc_and_plot_graph(X, interval, "tolygiai")

I = np.arange(n)
a, b = interval
X = (b + a) / 2 + (b - a) / 2 * np.cos(np.pi * (2 * I + 1) / (2 * n))

calc_and_plot_graph(X, interval, "ciobysevo")
