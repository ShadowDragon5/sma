'''
Čiobyševo metodas
'''
import numpy as np

class Chebyshev:
    def __init__(self, X, Y):
        self.n = len(X)                                 # daugianario eilė
        self.a = self._calculate_coeficients(X, Y).T    # koeficientai

    def _calculate_coeficients(self, X, Y):
        T = np.matrix([self._polynomial(x) for x in X])
        return np.linalg.solve(T, Y)

    def _polynomial(self, x):
        T = [1, x]
        for i in range(self.n - 2):
            T.append(2 * x * T[i+1] - T[i])
        return T

    def eval(self, X):
        T = np.matrix([self._polynomial(x) for x in X])
        yval = T @ self.a
        return yval.A1
