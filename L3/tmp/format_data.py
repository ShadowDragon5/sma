import shapefile as shp
import matplotlib.pyplot as plt

sf = shp.Reader("ne_10m_admin_0_countries")

country_id = [i for i, x in enumerate(sf.records()) if x.NAME_EN == 'Austria'][0]

''' naudojimui nerasant i atskirus failus
X, Y = zip(*sf.shape(country_id).points)
plt.scatter(X, Y)
plt.show()
'''

with open("X.csv", 'w') as X, open("Y.csv", 'w') as Y:
    for x, y in sf.shape(country_id).points:
        X.write(f"{x};")
        Y.write(f"{y};")

print("Job's done!")
