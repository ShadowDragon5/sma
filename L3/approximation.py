'''
IV užduotis. Aproksimavimas
'''
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

class Approximate:

    def __init__(self, X, Y, n):
        self.n = n
        Y = np.matrix(Y).T
        G = np.matrix([X ** d for d in range(self.n + 1)]).T
        self.c = np.linalg.solve(G.T @ G, G.T @ Y)

    def eval(self, X):
        G = np.matrix([X ** d for d in range(self.n + 1)]).T
        yval = G @ self.c
        return yval.A1


df = pd.read_csv("data/tas_1991_2016_AUT.csv", sep=', ', engine='python')

Y = df[df.Year == 2014].Temperature.to_numpy()
X = np.arange(1, len(Y)+1)

smooth = np.linspace(1, len(Y), 1000)

for n in [2, 3, 4, 5]:
    aprx = Approximate(X, Y, n)
    print(n, aprx.c)

    plt.clf()
    plt.scatter(X, Y, c='crimson')
    plt.plot(smooth, aprx.eval(smooth))

    plt.grid()
    plt.title(f"{n} laipsnio daugianaris")
    plt.xlabel("Mėnuo")
    plt.ylabel("Temperatūra, $\degree$C")

    plt.tight_layout()
    plt.savefig(f"img/apprx{n}.png", dpi=300)
