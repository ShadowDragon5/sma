'''
Globalus splainas
'''
import numpy as np
from scipy import sparse

class GlobalSpline:
    def __init__(self, X, Y, periodic=False):
        self.X = np.array(X)
        self.Y = np.array(Y)
        self.d = np.diff(X).astype(float)

        d1 = self.d[:-1]
        d2 = self.d[1:]
        n = len(self.X)

        D = sparse.diags([d1 / 6, (d1 + d2) / 3, d2 / 6], [0, 1, 2],
                shape=(n-2, n)).A

        Y = (Y[2:] - Y[1:-1]) / d2 - (Y[1:-1] - Y[:-2]) / d1

        if (periodic):
            D = np.vstack((D,
                [self.d[0] / 3, self.d[0] / 6, *[0] * (n-4),
                    self.d[-1] / 6, self.d[-1] / 3],
                [1, *[0] * (n-2), -1]))

            Y = np.array([*Y,
                (self.Y[1] - self.Y[0]) / self.d[0] -
                (self.Y[-1] - self.Y[-2]) / self.d[-1],
                0])
        else:
            D = np.vstack((D,
                [1, *[0] * (n-1)],
                [*[0] * (n-1), 1]))

            Y = [*Y, 0, 0]

        self.f = np.linalg.solve(D, Y)

    def eval(self, n=50):
        X = []
        Y = []
        for i, xi in enumerate(self.X[:-1]):
            for x in np.linspace(xi, self.X[i+1], n):
                s = x - xi
                y = self.f[i] * (s ** 2) / 2 + \
                    (self.f[i + 1] - self.f[i]) * s ** 3 / (6 * self.d[i]) + \
                    s * (self.Y[i + 1] - self.Y[i]) / self.d[i] - \
                    self.f[i] * self.d[i] * s / 3 - \
                    self.f[i + 1] * self.d[i] * s / 6 + \
                    self.Y[i]

                X.append(x)
                Y.append(y)

        return X, Y
